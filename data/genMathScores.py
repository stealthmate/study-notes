import numpy as np
import scipy.stats as sps
import matplotlib.pyplot as plt

N = 400
rv = sps.norm(69.530, np.sqrt(206.699)).rvs(N).astype(int)
print("\n".join([str(r) for r in rv]))
