---
classoption: fleqn
geometry: margin=2cm
header-includes:
- \def\R{\mathbb{R}}
- \def\C{\mathbb{C}}
- \renewcommand{\vec}[1]{\textbf{#1}}
- \DeclareMathOperator{\tr}{tr}
...

