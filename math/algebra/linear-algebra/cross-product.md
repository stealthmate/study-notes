---
classoption: fleqn
geometry: margin=2cm
header-includes:
- \def\R{\mathbb{R}}
- \def\C{\mathbb{C}}
- \renewcommand{\vec}[1]{\textbf{#1}}
- \DeclareMathOperator{\tr}{tr}
...

# Cross Product

## Definition

In a basis with vectors $\vec{i}$, $\vec{j}$ and $\vec{k}$, the cross product between two vectors $\vec{a}$ and $\vec{b}$ is defined as:

$$
\vec{a} \times \vec{b} = \begin{vmatrix}
\vec{i} & \vec{j} & \vec{k} \\
a_1 & a_2 & a_3 \\
b_1 & b_2 & b_3
\end{vmatrix}
$$

## Triple Product (Scalar)

The scalar triple product is defined as:

$$
\vec a \cdot (\vec b \times \vec c) = (\vec a\times\vec b)\cdot\vec c = \vec b \cdot (\vec c \times\vec a) = -\vec b \cdot(\vec a\times\vec c) = \begin{vmatrix}
a_1 & a_2 & a_3 \\
b_1 & b_2 & b_3 \\
c_1 & c_2 & c_3
\end{vmatrix}
$$

## Triple Product (Vector)

$$
\begin{aligned}
\vec a\times(\vec b\times\vec c) = (\vec a\cdot\vec c)\vec b - (\vec a\cdot\vec b)\vec c
\\
(\vec a\times\vec b)\times\vec c = (\vec a\cdot\vec c)\vec b - (\vec b\cdot\vec c)\vec a
\end{aligned}
$$
