---
classoption: fleqn
geometry: margin=2cm
header-includes:
- \setlength{\mathindent}{0pt}
- \def\R{\mathbb{R}}
- \def\C{\mathbb{C}}
- \renewcommand{\vec}[1]{\textbf{#1}}
...

# Vector Spaces
## Definition
A vector space over a field $F$ is a set $V$ together with two operations:

- **Addition** or **vector addition**: $+ : V \times V \to V$
- **Scalar multiplication**: $\cdot : F\times V \to V$

Elements of $V$ are called **vectors**, while elements of $F$ are called **scalars**.

| Property | Meaning |
|--|--|
| Associativity of addition | $\vec{u} + (\vec{v} + \vec{w}) = (\vec{u} + \vec{v}) + \vec{w}$ |
| Commutativity of addition | $\vec{u} + \vec{v} = \vec{v} + \vec{u}$ |
| Additive identity (**zero vector**) | $\exists \vec{0}\in V, \,\, \vec{v} + \vec{0} = \vec{v} \forall \vec{v} \in V$ |
| Additive inverses (**additive inverse**) | $\forall \vec{v}\in V \,\, \exists \vec{-v} \in V, \, \vec{v} + (\vec{-v}) = \vec{0}$ |
| Compatibility of scalar with field multiplication | a(b\vec{v}) = (ab)\vec{v}  |
| Scalar multiplicative identity | $1\vec{v} = \vec{v}$ |
| Distributivity of scalar multiplication over vector addition | a(\vec{u} + \vec{v}) = a\vec{u} + a\vec{v} |
| Distributivity of scalar multiplication over field addition | (a+b)\vec{v} = a\vec{v} + b\vec{v} |

## Examples

### Real Vectors ($\R^n$)

### Complex Vectors ($\mathbb{C}^n$)

### $F^{m\times n}$ Matrices