---
classoption: fleqn
geometry: margin=2cm
header-includes:
- \def\R{\mathbb{R}}
- \def\C{\mathbb{C}}
- \renewcommand{\vec}[1]{\textbf{#1}}
- \newcommand{\inner}[2]{\langle{#1}\,,{#2}\rangle}
- \DeclareMathOperator{\tr}{tr}
...

# Inner Product Spaces

## Definition

A vector space $V$ over a field $F$, together with an *inner product* operation
$$
\langle\cdot ,\cdot\rangle : V \times F \to F
$$

that satisifed the following axioms is called an **inner product space**.

- **Conjugate symmetry**:
$$
\inner{\vec{u}}{\vec{v}} = \overline{\inner{\vec{v}}{\vec{u}}}
$$

- **Linearity in the first argument**:
$$
\begin{aligned}
&\inner{a\vec{u}}{\vec{v}} = a\inner{\vec{u}}{\vec{v}} \\
&\inner{\vec{u} + \vec{v}}{\vec{w}} = \inner{\vec{u}}{\vec{w}} + \inner{\vec{v}}{\vec{w}}
\end{aligned}
$$

- **Positive definite**:
$$
\begin{aligned}
&\inner{\vec{v}}{\vec{v}} \ge 0 \\
&\inner{\vec{v}}{\vec{v}} = 0 \iff \vec{v} = \vec{0}
\end{aligned}
$$

## Properties
$$
\begin{aligned}
& \inner{\vec{v}}{\vec{v}} = \overline{\inner{\vec{v}}{\vec{v}}} \\
& \inner{-\vec{v}}{\vec{v}} = \inner{\vec{v}}{-\vec{v}} \\
& \inner{\vec{v}}{a\vec{u}} = \overline{\inner{a\vec{v}}{\vec{u}}} = \overline{a}\inner{\vec{v}}{\vec{u}} \\
& \inner{\vec{v}}{\vec{u} + \vec{w}} = \overline{\inner{\vec{u} + \vec{w}}{\vec{v}}} = \overline{\inner{\vec{u}}{\vec{v}}} + \overline{\inner{\vec{w}}{\vec{v}}} = \inner{\vec{v}}{\vec{u}} + \inner{\vec{v}}{\vec{w}}
\end{aligned}
$$

## Examples

### Real Vectors
### Complex Vectors
### Complex Matrices

We can define an inner product for all square matrices as the following:
$$
\begin{aligned}
& \overline{\vec{A}} =  \vec{A}^{*} \\
& \inner{\vec{A}}{\vec{B}} = \tr(\vec{A}\vec{B}^{*})
\end{aligned}
$$
