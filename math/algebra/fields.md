---
classoption: fleqn
geometry: margin=2cm
header-includes:
- \setlength{\mathindent}{0pt}
- \def\R{\mathbb{R}}
- \def\C{\mathbb{C}}
---

# Fields

## Definition

A field is a **set** $F$, equipped with two operations - **addition** and **multiplication**, which satisfies the following axioms:

- *Associativity*
$$
\begin{aligned}
a + (b + c) &= (a + b) + c \\
a \cdot (b \cdot c) &= (a \cdot b) \cdot c
\end{aligned}
$$
- *Commutativity*
$$
\begin{aligned}
a + b &= b + a \\
 a \cdot b &= b \cdot a
\end{aligned}
$$
- *Additive and Multiplicative Identity*
$$
\begin{aligned}
&\exists 0 \implies a + 0 = a \\
&\exists 1 \implies a \cdot 1 = a
\end{aligned}
$$
- *Additive Inverses*
$$
\forall a \in F, \, \exists (-a) \implies a + (-a) = 0
$$
- *Multiplicative Inverses*
$$
\forall a \in F, \, \exists (a^{-1}) \implies a \cdot a^{-1} = 1
$$
- *Distributivity*
$$
a \cdot (b + c) = (a \cdot b) + (a\cdot c)
$$

This can be summed up by saying that:

*A field has two operations - addition and multiplication. It is an abelian group under addition with $0$ and an abelian group under multiplication with $1$, and multiplication is distributive over addition.*

## Examples
- Real Numbers $\R$
- Complex Numbers $\C$
