---
classoption: fleqn
geometry: margin=2cm
header-includes:
- \newenvironment{rcases}
  {\left.\begin{aligned}}
  {\end{aligned}\right\rbrace}
---

# Semantic Tree

A semantic tree is a case-by-case analysis of a statement for each variable. That is, for a statement like:

$$
p \land q \to (r \lor s)
$$

A semantic tree would recursively evaluate all the possible combinations of values for $p$, $q$, $r$ and $s$.

# Semantic Proof

An inferral is an implication that should be always true. That is, we have *premises* and *conclusions*. For example:
$$
p, q \vdash r
$$

This would be read as:
$$
\text{Given } p \text{ and } q \text{, } r \text{ is provable.}
$$

where $p$ and $q$ are premises and $r$ is the conclusion. As a single formula, this can be written like:
$$
(p \land q) \to r = {\sim (p\land q) \lor r}
$$

Therefore, if we want to prove the inferral, we have to show that the above is true for any value of the literals - that is, it is a **tautology**. The inverse of a tautology is called a **contradiction**. We can show that something is a tautology by constructing a semantic tree -- if we get $T$ at all the leaves, then that is a tautology.
