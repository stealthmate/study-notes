---
classoption: fleqn
geometry: margin=2cm
header-includes:
- \newenvironment{rcases}
  {\left.\begin{aligned}}
  {\end{aligned}\right\rbrace}
---

# Inference Axioms

$$
\begin{aligned}
P &\to (Q \to P) \\
(P \to (Q \to R)) &\to ((P\to Q)\to(P\to R)) \\
(P \to Q) &\to ((P \to {\sim Q}) \to{\sim P})
\end{aligned}
$$

# Formal Proof

A formal proof is a sequence of sentences, each of which is an axiom, assumption or follows from preceding sentences by a rule of inference.

Formal proofs are constructed by applying the inference axioms and the rule of inference:
$$
P, P \to Q \vdash Q
$$

An example of a formal proof would be:
$$
\text {Prove: } {\sim p} \vdash p \to {\sim q}
$$


| Sentence                        | Explanation |
|:-------------------------------:|:-----------:|
| $\sim q$                        | assumption  |
| ${\sim q} \to (p \to {\sim q})$ | Axiom 1     |
| $p \to {\sim q}$                | Inference   |

# Deduction Theorem

The deduction theorem is useful whenever we have to prove conclusions that are implications. It states that:
$$
(p \vdash q \to r) \equiv (p, q \vdash r)
$$

Using this, we can prove $p\to q, q\to r \vdash p \to r$:

| Sentence                      | Explanation |
|:-----------------------------:|:-----------:|
| $p \to q, q\to r, p \vdash r$ | Deduction   |
| $q\to r$                      | Assumption  |
| $p \to (q\to r)$              | Axiom 1     |
| $(p\to q)\to(p\to r)$         | Axiom 2     |
| $p\to r$                      | Inference   |
| $p$                           | Assumption  |
| $r$                           | Inference   |
