---
classoption: fleqn
geometry: margin=2cm
header-includes:
- \newenvironment{rcases}
  {\left.\begin{aligned}}
  {\end{aligned}\right\rbrace}
---
# Introduction

## Definitions

In propositional logic we deal with **propositions** or **statements** - that is, things which can be either $\sc{True}$ or $\sc{False}$.

We can combine propositions by utilizing the operands of boolean algebra:

| Operand     | Meaning      |
|:-----------:|--------------|
| $p \lor q$  | $p$ or $q$   |
| $p \land q$ | $p$ and $q$  |
| $\sim p$ | not $p$ |

The above operands satisfy the following laws:

|                  | $\lor$                                    | $\land$                                    |
|:----------------:|:-----------------------------------------:|:------------------------------------------:|
| Identity         | $p \lor p = p$                            | $p \land p = p$                            |
| Commutativity    | $p\lor q = q \lor p$                      | $p \land q = q \land p$                    |
| Associativity    | $p \lor (q \lor r) = (p \lor q) \lor r$   | $p\land(q\land r)=(p\land q)\land r$       |
| Distributivity   | $p\lor(q\land r)=(p\lor q)\land(p\lor r)$ | $p\land(q\lor r)=(p\land q)\lor(p\land r)$ |
| Excluded Middle  | $p \lor {\sim p} = T$                     |                                            |
| Noncontradiction |                                           | $p \land{\sim p} = F$                       |

Using the above three, we can derive two more useful operands:
$$
\begin{aligned}
p \to q &= \sim p \lor q \\
p \leftrightarrow q &= (p \to q) \land (q \to p) = (\sim p \lor q) \land (\sim q \lor p)
\end{aligned}
$$

## Truth Tables

A logic statement's value can be evaluated using a *truth table*. For example, the truth table of $p \to q$ would be:

| $p$ | $q$ | $p\to q$ |
|:---:|:---:|:--------:|
| 0   | 0   | 1        |
| 0   | 1   | 1        |
| 1   | 0   | 0        |
| 1   | 1   | 1        |

## Further Theorems

Using the above laws, we can derive the following additional properties:

$$
\begin{aligned}
&T \land p = p &T \lor p = T\\
&F \land p = F &F \lor p = p \\
&\sim\sim p = p
\end{aligned}
$$

\newpage

We can also derive the **Absorbption Law*:

$$
\begin{aligned}
p \lor (p \land q) &= (p \land T) \lor (p \land q) \\
&= p \land (T \lor q) \\
&= p \land T \\
&= p
\end{aligned}
$$

and the De Morgan Laws:
$$
\begin{aligned}
\sim (p \land q) &= {\sim p}\lor{\sim q} \\
\sim (p \lor q) &= {\sim p}\land{\sim q}
\end{aligned}
$$ **NEEDS PROOF**


