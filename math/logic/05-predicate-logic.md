---
classoption: fleqn
geometry: margin=2cm
header-includes:
- \newenvironment{rcases}
  {\left.\begin{aligned}}
  {\end{aligned}\right\rbrace}
---

# Predicates

A predicate is a function which takes *variables* or *constraints* and returns either $T$ or $F$. A $n-$order logic is one where predicates take $n$ arguments. A *term* is either a constraint or a function that returns a constraint (not to be confused with predicate).

# Propositional Logic

A propositional logic consists of *terms* and *formulas*. A formula is defined either of the following:

- A predicate $P$ on the terms $t_1, t_2, ..., t_n$ --- $P(t_1, t_2, ..., t_3)$
- Any logical operation on one or more formulas --- $P, P\land Q, P\to Q$, etc.
- A predicate preceded by a quantifier --- $\forall x P$ or $\exists x P$

A *quantifier* is something that specifies the subdomain of specimens that satisfy a given predicate. The two most commonly used ones are $\forall$ (for all) and $\exists$ (exists).

# Scope of Quantifiers

The scope of the quantifier is the formula which it describes.

Quantifier operands have higher precedence than other operands, meaning that:
$$
\forall x P(x)\land Q(y) \equiv (\forall x P(x)) \land Q(y)
$$

- A **bound variable** is one with a quantifier
- A **free variable** is one without a quantifier
- A **closed forumla** is one where there are no free variables

# Properties of Quantifiers

We can think of $\forall$ as an *and* expression over the whole domain --- $P(x_1)\land P(x_2) \land ...$ and we can think of an $\exists$ as an *or* expression. If we apply De Morgan here, we can derive the following:

$$
\begin{aligned}
\sim \forall x P(x) &\equiv \exists x\, {\sim P(x)} \\
\sim \exists x P(x) &\equiv \forall x\, {\sim P(x)}
\end{aligned}
$$

# Prenex Normal Forms

Similar to propositional logic, predicate logic also has CNF and DNF. The difference here is, that in prenex CNF/DNF all the quantifiers have to come at the beginnging of the formula. That is, the end result must look like:
$$
\begin{aligned}
& Qx_1\; Qx_2\;...\;Qx_n((l_1 \lor l_2)l_3)  &\text{for CNF} \\
& Qx_1\; Qx_2\;...\;Qx_n(l_1 \lor l_2l_3)  &\text{for DNF}
\end{aligned}
$$
