---
classoption: fleqn
geometry: margin=2cm
header-includes:
- \newenvironment{rcases}
  {\left.\begin{aligned}}
  {\end{aligned}\right\rbrace}
---
# Types of Propositions

### Literal
A literal is a single propositional variable, either by itself -- $p$ -- or negated -- $\sim p$.

### Term
A term is a *conjunction* of one or more literals -- $p \land q$. It is never negated.

### Clause
A clause is a *disjunction* of one or more literals -- $p \lor q$. It is never negated.

# Normal Forms

Logical statements can be simplified to one (or both) of two normal forms - **Conjunctive (CNF)** and **Disjunctive (DNF)**. 

CNF is defined as a conjunction of disjunctions:
$$
(p \lor q) \land (r \lor s)
$$

DNF is defined as a disjunction of conjunctions:
$$
(p \land q)\lor(r \land s)
$$

Note that a clause/term by itself can be considered both CNF and DNF.

Normal forms are mostly derived by using De Morgan's laws.
