{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Probability Basics\n",
    "\n",
    "## Definitions\n",
    "\n",
    "Consider a die. We cannot know what side it falls on until we throw it, however we do know that it has 6 sides and that there is an equal chance of it falling on any one of them.\n",
    "\n",
    "Whenever we have such a case, where we cannot tell the result, but know the probability of all the possible results, we have a **random variable**. Observing the result of a random variable is called a **trial**, while the observed result is called a **realization**. The possible result of a trial - the die falling on 3, on 5, on an even number, etc. - is called an **event** and events which cannot be broken down are called **elementary events**. Possibility is defined for *events* and (for a die) is written like this:\n",
    "\n",
    "$$\n",
    "P(X = 1) = \\frac{1}{6}\n",
    "$$\n",
    "\n",
    "The probability of event of a die falling on an even number would be:\n",
    "\n",
    "$$\n",
    "P((X = 2) \\cup (X = 4) \\cup (X = 6)) = P(X = 2) + P(X = 4) + P(X = 6) = 3\\times\\frac{1}{6} = \\frac{1}{2}\n",
    "$$\n",
    "\n",
    "Two events are **mutually exclusive** if they cannot occur at the same time."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Probability Distribution\n",
    "\n",
    "A probability distribution is a mapping of all realizations of a random variable to a number between $0$ and $1$ that signifies their probability, where the combined probability of all realizations adds up to $1$.\n",
    "\n",
    "In other words, for a set of values $\\mathbb{A}$ of a random variable $X$, the probability distribution is defined as:\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "&f: \\mathbb{A} \\to \\mathbb{R} \\\\\n",
    "&\\sum_{x\\in\\mathbb{A}}{P(X = x)} = 1\n",
    "\\end{aligned}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example\n",
    "\n",
    "Let's see how the probability distribution forms when we have a biased die with probabilities in rising in order of the faces."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAA6sAAAE/CAYAAACgilX5AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDMuMC4yLCBodHRwOi8vbWF0cGxvdGxpYi5vcmcvOIA7rQAAIABJREFUeJzt3X+w3fdd3/nnC3ntpAkpSq3ZmVhS7ASZRN6wcblxZifbREtsR16KzMw6g+1Jx+mmVbMbL9QuA85Cbao0nRB2MMyuAWuoSss2COOUrqYVdbXEYpdNDVISk4xs5MjCtW4EjUAOIcTYlfPeP873ykfXV9L3/jj3fs45z8fMGX9/fD7f7/tIfs9H7++Pz0lVIUmSJElSS75trQOQJEmSJGk+i1VJkiRJUnMsViVJkiRJzbFYlSRJkiQ1x2JVkiRJktQci1VJkiRJUnMsVrUkSX4xyT/s2fZgkr8z6pgkLSzJ5iTfSLKuR9ttSWZXIy5JC3OMlcaL4+zoWKyOiSR3Jjmc5IUkv7zA/vcm+YMk30zyaJI3Du27LMmeJF9P8sdJ7r7AeT6Y5HcuFk9VfbiqPrbkLySNuVHm5IX6LnCeZ5Jcf6FYq+rZqnptVb20hK8qjYW1yknHWOn8Ws1Lx9nxYbE6Pk4C/xjYM39HksuBfwX8Q+D1wGHg14aa/CSwBXgj8N8BP5pk+1ID6XPVSJoCI8nJHn0XJcklS+0rjZm1ysnz9l0Kx1hNmOby0nF2zFSVnzH6MEj4X563bSfw2aH11wDPA2/p1r8C3Di0/2PA3gWO/VbgL4GXgG8AX+u2/zLwC8B+4C+A67tt/7jbvx74N8Ap4LlueePQcQ8Cf6db/k7gt4E/A/4E+LW1/jP142c5n5XOyYv1nXeeXwG+1e3/BvCjwJVAAR8CngX+n6Ftl3T9/jbwJPDnwHHg7w0dcxswO7T+Y128fw4cBd671n/mfvxc6LPaOekY68fPxT8t5eXF+s6L0XF2jT/eWZ0M1wC/P7dSVX8BPA1ck2Q98Ibh/d3yNfMPUlVPAh8G/kMNHmX4jqHdtwMfB74dmP8I07cB/4zB1avNDBL6/zhPrB8D/j2DwXcj8L/3+4rSWFlOTp637/yTVNXfYjBQfn+Xs58c2v0eBv84ft8C8X0V+JvA6xgMqPcn+evzGyX5LuBO4B1V9e3dsZ650BeXGjWSnHSMlZZlrfLScXaMeNt6MryWwRXXYX/GYNB77dD6/H2L8X9V1f/XLf9lkrM7qupPgU/PrSf5OPDoeY7znxkMuG+oqlleOShLk2A5OXmhvovxk90AzHC+AlTVvx1a/e0k/x74G8Dn5x3jJeAyYGuSU1X1zCJjkFoxqpx0jJWWbq3y0nF2jHhndTJ8g8GVm2GvY/A4wTeG1ufvW4wT59uR5K8keTDJf0zydQaPQ3zHed67+VEgwO8lOZLkf1xkHNI4WE5OXqjvYlwoZ29K8liS00m+Bvz3wOXz21XVMeDvM3j356tJ9iZ5wyLjkFowqpx0jJWWbq3y0nF2jFisToYjwH89t5LkNcCbgSNV9RzwR8P7u+Uj5zlWLXI7wD8Avgt4Z1W9Dnj3XCivOEjVH1fV362qNwB/D/j5JN95gWNL42g5OXnevuc516JyNsllDO7S/G/Af9k9irifBfIVoKo+VVX/LYO7NQX81HnOJ7VsJDnpGCsty1rlpePsGLFYHRNJLknyKmAdsC7Jq4ZmH/sN4L9K8j90be4FvlhVf9Dt/xfATyRZn+QtwN9lMHnDQv4TsDHJpYsI79sZvEPztSSvB+67wPd4f5KN3epzDJLSab41dkaYkxfrO99/At60iNAvZfDI0SngTJKbgBvP8x2/K8n3dgPvXzLIc/NVTVrDnHSMlc6j0bx0nB0naz3Dk59+HwaPB9S8z08O7b8e+AMG/5MfBK4c2ncZgynDv84g4e6+wHkuBf4tcBr4k27bL9PNSjjU7uw2Bi+xH2TwWMVTDK7mDs+IdpCXZyr8JIMZz77B4GX2nWv9Z+vHz1I+o8zJC/VdII6bGUz+8DXgR5g3I2HX5pxtwEe6836NwUyHe4fyeRvdLIXAdwO/x+DRqNMMZiF9w1r/2fvxs9BnrXLSMdaPn/N/Ws3LC/Vd4Ds4zq7hJ90flCRJkiRJzfAxYEmSJElScyxWJUkasSTbkxxNcizJPQvs/3CSLyV5PMnvJNk6tO+jXb+jSRb6PT9Jy2SOSm3yMWBpAiTZDvwcgwkMfqmqPjFv/4cZvD/xEoN3mXZW1RPdvo8CH+r2/VBVPbKasUuTrvuJkaeAG4BZ4BBw21wOdm1eV1Vf75Z3AP9zVW3v/kH8q8B1DN5d/L+Bq6vKCTikFWKOSu3yzqo05rpB9gHgJmArcNvwFd/Op6rqbVX1dgYTcPxM13crcCtwDbCdwc8cLPTbfZKW7jrgWFUdr6oXGUy0cfNwg7l/BHdew8s/iXAzsLeqXqiqPwSOdceTtHLMUalRFqvS+HOQldp2Bef+ePxst+0cST6S5GkGF5R+aDF9JS2LOSo16pKLN1ldl19+eV155ZVrHYa05j73uc/9SVVt6NF0oYHynfMbJfkIcDeDn0743qG+j83re8FB1hyVFpWfsPAPwb/iHZyqegB4IMntwE8Ad/Ttm2QnsBPgNa95zfe85S1v6RmaNJnMUaltfXO0uWL1yiuv5PDhw2sdhrTmkvzHvk0X2DayQXbz5s3mqKbeIvITBheBNg2tbwROXqD9XuAXFtO3qnYDuwFmZmbKHNW0M0eltvXN0V6PAS91hrQkVyZ5vtv+eJJfXNzXkNTDUgbZH1hM36raXVUzVTWzYUPfC9WSOoeALUmuSnIpg/fE9w03SLJlaPX7gC93y/uAW5NcluQqYAuDH5CXtHLMUalRF72zOjR5y9kZ0pLsG54hjcHkLb/Ytd/BYPKW7d2+p7tJXSSNxtlBFvgKg0H29uEGSbZU1dzAOn+Q/VSSn2Ewi6GDrLTCqupMkjuBRxjM2L2nqo4k2QUcrqp9wJ1Jrgf+M/Acgycf6No9BDwBnAE+4iyj0soyR6V29XkM+OzkLQBJ5iZvOVusXmDyFkkj5iArta+q9gP75227d2j5hy/Q9+PAx0cXnSRzVGpTn2J1OZO3AFyV5AvA14GfqKr/d4G+57wPJ2lxHGQlSZI0afq8s9p78paqejPwYwwmbwH4I2BzVV3LoJD9VJLXLdDX9+EkSZIkSWf1KVaXPHlL99uNf9otfw54Grh6aaFKkiRJkqZFn2J1yTOkJdnQTdBEkjcxmLzl+EoELkmSJEmaXBd9Z3U5k7cA7wZ2JTkDvAR8uKpOj+KLSJIkSZImR58JlpY8eUtVfRr49HIClCRJkiRNnz6PAUuSJEmStKp63VmVJEmSXmHbtrU578GDa3NeadysRY6uYH5arEqd+w88NdLj33WDE2FLizY0yJ547vmRnmrT+le/vOI/hCVJE2aU4+g5Y+gKsliVJEnS0nhhR2rbUI4+PMIbM6O6KWOxKklq1yoNsuDTD9Jy+YSSpJXmBEuSJEmSpOZYrEqSJEmSmmOxKkmSJElqjsWqJEmSJKk5FquSJEmSpOZYrEqSJEmSmmOxKkmSJElqjsWqJEmSJKk5FquSJEmSpOZYrEqSJEmSmmOxKkmSJElqjsWqJEmSJKk5FquSJEmSpOZYrEqSJEmSmmOxKkmSJElqjsWqJEmSJKk5FquSJEmSpOZYrEqSJEmSmmOxKkmSJElqjsWqJEmSJKk5FquSJEmSpOZYrEqSJEmSmmOxKkmSJElqjsWqJEmSJKk5vYrVJNuTHE1yLMk9C+z/cJIvJXk8ye8k2Tq076Ndv6NJ3reSwUuSJEmSJtNFi9Uk64AHgJuArcBtw8Vo51NV9baqejvwSeBnur5bgVuBa4DtwM93x5MkaWr0uOh7d5InknwxyW8leePQvpe6i8GPJ9m3upFL08EcldrU587qdcCxqjpeVS8Ce4GbhxtU1deHVl8DVLd8M7C3ql6oqj8EjnXHkyRpKvS86PsFYKaqvht4mMGF3znPV9Xbu8+OVQlamiLmqNSuPsXqFcCJofXZbts5knwkydMMkveHFtl3Z5LDSQ6fOnWqb+ySOl4RlprW56Lvo1X1zW71MWDjKscoTTNzVGpUn2I1C2yrV2yoeqCq3gz8GPATi+y7u6pmqmpmw4YNPUKSNMcrwlLzel24HfIh4DeH1l/VXdB9LMkPjCJAacqZo1KjLunRZhbYNLS+ETh5gfZ7gV9YYl9Ji3f2ijBAkrkrwk/MNaiqR4faPwZ8YFUjlKZbrwu3AEk+AMwA7xnavLmqTiZ5E/CZJF+qqqfn9dsJ7ATYvHnzykQtTQ9zVGpUnzurh4AtSa5KcimDCZPOeVQwyZah1e8Dvtwt7wNuTXJZkquALcDvLT9sSUNGfkXYR/WlZel14TbJ9cCPAzuq6oW57VV1svvvceAgcO38vj6hJC2LOSo16qLFalWdAe4EHgGeBB6qqiNJdiWZe2TwziRHkjwO3A3c0fU9AjzE4A7PvwM+UlUvjeB7SNNsKVeEf3po8+aqmgFuB342yZtfcTAHWWk5+lz0vRZ4kME/gr86tH19ksu65cuBdzH01ISkFWGOSo3q8xgwVbUf2D9v271Dyz98gb4fBz6+1AAlXdRirwi/53xXhJMcZHBF+On5/SUtTVWdSTJ30XcdsGfuoi9wuKr2MbiA9Frg15MAPNu9Q/5W4MEk32JwgfkTVeU/hKUVZI5K7epVrEpq2tkrwsBXGFwRvn24wdAV4e3zrwgD36yqF4auCA9PviRpBfS46Hv9efp9FnjbaKOTZI5KbbJYlcacV4QlSZI0iSxWpQngFWFJkiRNGotVSdKi3X/gqZEd+64brh7ZsSVJ0viwWNV027bt7OItzz0/2nOtf/XLywcPjvZckiRJ0pjr8zurkiRJkiStKu+saroN3eF8eISPNYKPNkqSRmzoaaFV45NCUn9DOXpihE/0bZqgp/ksViVJ/azWY/MTNMhKkqSls1iVJEmaBF7ckdq2Sk/0TdLTfBarkqR+HGQlSdIqcoIlSZIkSVJzvLMqSZI0YfwtZEmTwDurkiRJkqTmWKxKkiRJkppjsSpJkiRJao7FqiRJkiSpORarkiRJkqTmWKxKkiRJkppjsSpJkiRJao7FqiRJkiSpORarkiRJkqTmWKxKkiRJkppjsSpJkiRJao7FqiRJkiSpORarkiRJkqTmWKxKkiRJkppjsSpJkiRJao7FqiRJkiSpORarkiRJkqTm9CpWk2xPcjTJsST3LLD/7iRPJPlikt9K8sahfS8lebz77FvJ4CVJkiRJk+mSizVIsg54ALgBmAUOJdlXVU8MNfsCMFNV30zyPwGfBH6w2/d8Vb19heOWJEmSJE2wPndWrwOOVdXxqnoR2AvcPNygqh6tqm92q48BG1c2TEmSJEnSNOlTrF4BnBhan+22nc+HgN8cWn9VksNJHkvyAwt1SLKza3P41KlTPUKSJEmSJE2yiz4GDGSBbbVgw+QDwAzwnqHNm6vqZJI3AZ9J8qWqevqcg1XtBnYDzMzMLHhsSZIkSdL06HNndRbYNLS+ETg5v1GS64EfB3ZU1Qtz26vqZPff48BB4NplxCtJkiRJmgJ9itVDwJYkVyW5FLgVOGdW3yTXAg8yKFS/OrR9fZLLuuXLgXcBwxMzSZIkSZL0Chd9DLiqziS5E3gEWAfsqaojSXYBh6tqH/DTwGuBX08C8GxV7QDeCjyY5FsMCuNPzJtFWJIkSZKkV+jzzipVtR/YP2/bvUPL15+n32eBty0nQEmSJEnS9OnzGLAkSVqGJNuTHE1yLMk9C+y/O8kTSb6Y5LeSvHFo3x1Jvtx97ljdyKXpYI5KbbJYlSaAg6zUriTrgAeAm4CtwG1Jts5r9gVgpqq+G3gY+GTX9/XAfcA7Gfzu+X1J1q9W7NI0MEeldlmsSmPOQVZq3nXAsao6XlUvAnuBm4cbVNWjVfXNbvUxBjPvA7wPOFBVp6vqOeAAsH2V4pamhTkqNarXO6vSqti2bfXPefDg6p9z5Z0dZAGSzA2yZyczq6pHh9o/BnygWz47yHZ95wbZX12FuKVpcQVwYmh9lsEFovP5EPCbF+h7xYpGp5G6/8BTIz3+XTdcPdLjTwlzVGqUxao0/kY+yCbZCewE2Lx583JilaZRFthWCzZMPgDMAO9ZTF9zVFoWc1RqlMWq2jEZdznXwsgH2araDewGmJmZWfDYks5rFtg0tL4RODm/UZLrgR8H3lNVLwz13Tav78H5fc1RaVnMUalRvrMqjb/FDrI75g2yF+0raVkOAVuSXJXkUuBWYN9wgyTXAg8yyM+vDu16BLgxyfruffIbu22SVo45KjXKYlUafw6yUsOq6gxwJ4PcehJ4qKqOJNmVZEfX7KeB1wK/nuTxJPu6vqeBjzHI80PArrl3zCWtDHNUapePAUtjrqrOJJkbZNcBe+YGWeBwVe3j3EEW4Nmq2lFVp5PMDbLgICuNRFXtB/bP23bv0PL1F+i7B9gzuugkmaNSmyxWpQngICtJkqRJ42PAkiRJkqTmeGdVkiRpJQ39bvgtzz0/2nOtf/XLy86qL/UzlKOryhxdNItVSRo3DrKSJK2IEyO+oLRp+IKSFs1iVZIkaSUNXdh5+MBTIz3VXTdcPdLjSxPJHB0bFquSNG68wylJkqaAEyxJkiRJkppjsSpJkiRJao6PAatJ94/w/QHfHZAkSZLa551VSZIkSVJzLFYlSZIkSc2xWJUkSZIkNcdiVZIkSZLUHItVSZIkSVJzLFYlSZIkSc2xWJUkSZIkNcdiVZIkSZLUHItVSZIkSVJzLFYlSZIkSc2xWJUkSZIkNadXsZpke5KjSY4luWeB/XcneSLJF5P8VpI3Du27I8mXu88dKxm8JEmSJGkyXbRYTbIOeAC4CdgK3JZk67xmXwBmquq7gYeBT3Z9Xw/cB7wTuA64L8n6lQtfkiRJkjSJLunR5jrgWFUdB0iyF7gZeGKuQVU9OtT+MeAD3fL7gANVdbrrewDYDvzq8kOXJN1/4KmRHv+uG64e6fElSZLOp89jwFcAJ4bWZ7tt5/Mh4DeX2FeSJEmSpF53VrPAtlqwYfIBYAZ4z2L6JtkJ7ATYvHlzj5C0WrxrI0mSJGkt9LmzOgtsGlrfCJyc3yjJ9cCPAzuq6oXF9K2q3VU1U1UzGzZs6Bu7JEmSJGlC9SlWDwFbklyV5FLgVmDfcIMk1wIPMihUvzq06xHgxiTru4mVbuy2SZIkSZJ0Xhd9DLiqziS5k0GRuQ7YU1VHkuwCDlfVPuCngdcCv54E4Nmq2lFVp5N8jEHBC7BrbrIlSZIkSZLOp887q1TVfmD/vG33Di1ff4G+e4A9Sw1QkiRJkjR9+jwGLEmSJEnSqup1Z1VTZtu2s4u3PPf8aM+1/tUvLx88ONpzSZIkSRob3lmVJEmSJDXHO6t6paE7nA/7O6uSJEmS1oB3ViVJkiRJzbFYlSRJkiQ1x2JVkiRJktQci1VJkkYsyfYkR5McS3LPAvvfneTzSc4kuWXevpeSPN599q1e1NL0MEelNlmsShPAQVZqV5J1wAPATcBW4LYkW+c1exb4IPCpBQ7xfFW9vfvsGGmw0hQyR6V2ORuwNOaGBtkbgFngUJJ9VfXEULO5QfZHFjjE81X19pEHKk2v64BjVXUcIMle4GbgbI5W1TPdvm+tRYDSlDNHpUZZrErjz0G2EfeP8Kee/JmnsXYFcGJofRZ45yL6vyrJYeAM8Imq+tcrGZwkc1RqlcWqNP4cZKW2ZYFttYj+m6vqZJI3AZ9J8qWqevqcEyQ7gZ0AmzdvXnqk0nQyR6VG+c6qNP5WYpCdAW4HfjbJm19xgmRnksNJDp86dWqpcUrTahbYNLS+ETjZt3NVnez+exw4CFy7QJvdVTVTVTMbNmxYXrTS9DFHpUZZrErjz0FWatshYEuSq5JcCtwK9JrMLMn6JJd1y5cD72LoEX9JK8IclRplsSqNPwdZqWFVdQa4E3gEeBJ4qKqOJNmVZAdAknckmQXeDzyY5EjX/a3A4SS/DzzK4FF9c1RaQeao1C7fWZXGXFWdSTI3yK4D9swNssDhqtqX5B3AbwDrge9P8o+q6hoGg+yD3cRL34aDrDQSVbUf2D9v271Dy4cYPBUxv99ngbeNPEBpypmjUpssVqUJ4CArSeexbdvqn/PgwdU/pzSuzFFdgMWqJC3H0CB7y3PPj+4861/98rKDrCRpAp0Y4Ti6aXgc1diwWJUkSZPLiztS24Zy9GF/r1zzWKxK0nI4yEqSJI2EswFLkiRJkppjsSpJkiRJao6PAUuSpKlwv4/qS9JY8c6qJEmSJKk5FquSJEmSpOZYrEqSJEmSmmOxKkmSJElqjsWqJEmSJKk5FquSJEmSpOb0KlaTbE9yNMmxJPcssP/dST6f5EySW+bteynJ491n30oFLkmSJEmaXBf9ndUk64AHgBuAWeBQkn1V9cRQs2eBDwI/ssAhnq+qt69ArJIkSZKkKXHRYhW4DjhWVccBkuwFbgbOFqtV9Uy371sjiFGSJEmSNGX6PAZ8BXBiaH2229bXq5IcTvJYkh9YVHSSJEmSpKnU585qFthWizjH5qo6meRNwGeSfKmqnj7nBMlOYCfA5s2bF3FoSZIkSdIk6nNndRbYNLS+ETjZ9wRVdbL773HgIHDtAm12V9VMVc1s2LCh76ElSZIkSROqT7F6CNiS5KoklwK3Ar1m9U2yPsll3fLlwLsYetdVkiRJkqSFXLRYraozwJ3AI8CTwENVdSTJriQ7AJK8I8ks8H7gwSRHuu5vBQ4n+X3gUeAT82YRliRJkiTpFfq8s0pV7Qf2z9t279DyIQaPB8/v91ngbcuMcbpt27b65zx4cPXPKUmSJElD+jwGLEmSJEnSqup1Z1VryLuckiRJkqaQd1YlSZIkSc2xWJUkSZIkNcdiVZIkSZLUHItVSZIkSVJzLFYlSZIkSc2xWJUkSZIkNcdiVZIkSZLUHItVSZIkSVJzLFYlSZIkSc2xWJUkSZIkNeeStQ5Akkbh/gNPjezYd91w9ciOLUmSpAHvrEqSJEmSmmOxKknSiCXZnuRokmNJ7llg/7uTfD7JmSS3zNt3R5Ivd587Vi9qaXqYo1KbLFalCeAgK7UryTrgAeAmYCtwW5Kt85o9C3wQ+NS8vq8H7gPeCVwH3Jdk/ahjlqaJOSq1y2JVGnMOslLzrgOOVdXxqnoR2AvcPNygqp6pqi8C35rX933Agao6XVXPAQeA7asRtDRFzFGpUU6wNEZGOWEMOGnMGDs7yAIkmRtkn5hrUFXPdPvOO8h2++cG2V8dfdjS1LgCODG0PsvgAtFS+16xQnFJGjBHpUZ5Z1Uaf8sZKB1kpdHLAttqJfsm2ZnkcJLDp06dWlRwksxRqVUWq9L4c5CV2jYLbBpa3wicXMm+VbW7qmaqambDhg1LDlSaUuao1CgfA5bG33IH2W3z+h6c36iqdgO7AWZmZvoWwpIGDgFbklwFfAW4Fbi9Z99HgH8y9C75jcBHVz7E1eVrLWqMOSo1yjur0vg7O8gmuZTBILuvZ99HgBuTrO8G2hu7bZJWSFWdAe5kkFtPAg9V1ZEku5LsAEjyjiSzwPuBB5Mc6fqeBj7GIM8PAbvm3jGXtDLMUald3lmVxlxVnUkyN8iuA/bMDbLA4aral+QdwG8A64HvT/KPquqaqjqdZG6QhXEfZLdtO7t4y3PPj+4861/98vLBg6M7jyZGVe0H9s/bdu/Q8iEGTzYs1HcPsGekAa6G1cpPMEe1aOYo5+ToqjJHdQEWq9IEcJCVJEnSpLFYlTQ5hq7OPjzCd+J8H05aglXKTzBHpSUZylHfK1crfGdVkiRJktQci1VJkiRJUnMsViVJkiRJzbFYlSRJkiQ1p1exmmR7kqNJjiW5Z4H9707y+SRnktwyb98dSb7cfe5YqcAlSZIkSZProsVqknXAA8BNwFbgtiRb5zV7Fvgg8Kl5fV8P3Ae8E7gOuC/J+uWHLUmSJEmaZH3urF4HHKuq41X1IrAXuHm4QVU9U1VfBL41r+/7gANVdbqqngMOANtXIG5JkiRJ0gTrU6xeAZwYWp/ttvWxnL6SJEmSpCnVp1jNAtuq5/F79U2yM8nhJIdPnTrV89CSJEmSpEnVp1idBTYNrW8ETvY8fq++VbW7qmaqambDhg09Dy1JkiRJmlSX9GhzCNiS5CrgK8CtwO09j/8I8E+GJlW6EfjooqNsxbZtq3/OgwdX/5ySJEmStMYueme1qs4AdzIoPJ8EHqqqI0l2JdkBkOQdSWaB9wMPJjnS9T0NfIxBwXsI2NVtkyRJkiTpvPrcWaWq9gP75227d2j5EINHfBfquwfYs4wY2+FdTkmSJElaFX3eWZUkSZIkaVVZrEqSJEmSmtPrMWC90v0HnhrZse+64eqRHVuSJEmSxoF3ViVJkiRJzbFYlSRJkiQ1x2JVkiRJktQci1VJkiRJUnMsViVJkiRJzbFYlSRJkiQ1x2JVkiRJktQci1VJkiRJUnMsViVJkiRJzbFYlSRJkiQ1x2JVkiRJktQci1VJkiRJUnMsViVJkiRJzbFYlSRJkiQ1x2JVkiRJktQci1VJkiRJUnMsViVJkiRJzbFYlSRJkiQ1x2JVkiRJktQci1VpAiTZnuRokmNJ7llg/2VJfq3b/7tJruy2X5nk+SSPd59fXO3YpWlgjkptM0elNl2y1gFIWp4k64AHgBuAWeBQkn1V9cRQsw8Bz1XVdya5Ffgp4Ae7fU9X1dtXNWhpipijUtvMUald3lmVxt91wLGqOl5VLwJ7gZvntbkZ+Ofd8sPAe5NkFWOUppk5KrXNHJUaZbEqjb8rgBND67PdtgXbVNUZ4M+Av9btuyrJF5L8dpK/MepgpSlkjkptM0elRvkYsDT+FrqyWz3b/BGwuar+NMn3AP86yTVV9fVzOic7gZ0AmzdvXoGQpalijkptM0elRlmsSuNvFtg0tL4ROHmeNrNJLgH+KnC6qgp4AaCqPpfkaeBq4PBw56raDewGmJmZmT+Av9K2bUv5Hst38ODanFe6MHMUzE+1zBwFc1RNsliVxt8i7mOYAAAG9ElEQVQhYEuSq4CvALcCt89rsw+4A/gPwC3AZ6qqkmxgMNi+lORNwBbg+OqFLk0Fc1RqW9M5euK551fycOfYtP7VIzu2tBJ6FatJtgM/B6wDfqmqPjFv/2XAvwC+B/hT4Aer6pluWu8ngaNd08eq6sMrE7okGLw7k+RO4BEGObqnqo4k2QUcrqp9wD8FfiXJMeA0g4EY4N3AriRngJeAD1fV6WUH5dVZ6azWc/T+A08t+3AXctcNV4/0+NJytZ6jD48wR81Pte6ixarTeUvtq6r9wP552+4dWv5L4P0L9Ps08OmRByhNOXNUaps5KrWpz2zATuctSZIkSVpVfYpVp/OWJEmSJK2qPu+sOp23JEmSJGlV9SlWnc57jpPGSJIkSdKq6FOsNj2dt6S2OdOoJEmSluKixWrr03lLkiRJkiZPr99ZdTpvSZIkSdJq6jMbsCRJkiRJq8piVZIkSZLUHItVSZIkSVJzLFYlSZIkSc2xWJUkSZIkNcdiVZIkSZLUHItVSZIkSVJzLFYlSZIkSc2xWJUkSZIkNcdiVZIkSZLUnEvWOoDluv/AUyM9/l03XD3S40uSJEmSXsk7q5IkSZKk5lisSpIkSZKaY7EqSZIkSWqOxaokSZIkqTkWq5IkSZKk5lisSpIkSZKaY7EqSZIkSWqOxaokSZIkqTkWq5IkSZKk5lisSpIkSZKaY7EqSZIkSWqOxaokSZIkqTkWq5IkSZKk5lisSpIkSZKaY7EqSZIkSWqOxaokSZIkqTkWq5IkSZKk5lisSpIkSZKa06tYTbI9ydEkx5Lcs8D+y5L8Wrf/d5NcObTvo932o0net3KhS5pjjkptM0eltpmjUpsuWqwmWQc8ANwEbAVuS7J1XrMPAc9V1XcC9wM/1fXdCtwKXANsB36+O56kFWKOSm0zR6W2maNSu/rcWb0OOFZVx6vqRWAvcPO8NjcD/7xbfhh4b5J02/dW1QtV9YfAse54klaOOSq1zRyV2maOSo3qU6xeAZwYWp/tti3YpqrOAH8G/LWefSUtjzkqtc0cldpmjkqNuqRHmyywrXq26dOXJDuBnd3qN5Ic7RHXqrj73NXLgT9Z5XOumqHzrsr3nHfOVbMWf6cLnLePN/ZsZ46+bGJzdIz+v13pc7b6XfvmJ5ijc1r9uxzFOf2uoz/vxZijPYzJ3+Uozul3He05++iVo32K1Vlg09D6RuDkedrMJrkE+KvA6Z59qardwO4+Aa+lJIeramat4xi1afmeMDHf1RztTMjf50VNy/eEifmu5igT83fZi9917JijTMzfZS9+1/HR5zHgQ8CWJFcluZTBS+T75rXZB9zRLd8CfKaqqtt+azeD2lXAFuD3ViZ0SR1zVGqbOSq1zRyVGnXRO6tVdSbJncAjwDpgT1UdSbILOFxV+4B/CvxKkmMMrjLd2vU9kuQh4AngDPCRqnppRN9FmkrmqNQ2c1RqmzkqtSuDi0LqI8nO7jGOiTYt3xOm67tOg2n5+5yW7wnT9V0n3TT9XfpdNY6m6e/S7zo+LFYlSZIkSc3p886qJEmSJEmrymL1IpJsSvJokieTHEnyw2sd06glWZfkC0n+zVrHMkpJviPJw0n+oPv7/W/WOiYtnjk6uczRyWCOTi5zdDKYo5NrEnK0z0/XTLszwD+oqs8n+Xbgc0kOVNUTax3YCP0w8CTwurUOZMR+Dvh3VXVLN/vfX1nrgLQk5ujkMkcngzk6uczRyWCOTq6xz1HvrF5EVf1RVX2+W/5zBv9jX7G2UY1Oko3A9wG/tNaxjFKS1wHvZjC7H1X1YlV9bW2j0lKYo5PJHJ0c5uhkMkcnhzk6mSYlRy1WFyHJlcC1wO+ubSQj9bPAjwLfWutARuxNwCngn3WPgfxSktesdVBaHnN0opijE8gcnSjm6AQyRyfKROSoxWpPSV4LfBr4+1X19bWOZxSS/E3gq1X1ubWOZRVcAvx14Beq6lrgL4B71jYkLYc5OnHM0Qljjk4cc3TCmKMTZyJy1GK1hyT/BYPk/ZdV9a/WOp4RehewI8kzwF7ge5P8n2sb0sjMArNVNXfl8GEGCa0xZI5OJHN0gpijE8kcnSDm6ESaiBy1WL2IJGHwrPeTVfUzax3PKFXVR6tqY1VdCdwKfKaqPrDGYY1EVf0xcCLJd3Wb3gtM8kQCE8scNUfVNnPUHFXbzFFztGXOBnxx7wL+FvClJI932/7Xqtq/hjFpZfwvwL/sZkc7DvztNY5HS2OOTi5zdDKYo5PLHJ0M5ujkGvscTVWtdQySJEmSJJ3Dx4AlSZIkSc2xWJUkSZIkNcdiVZIkSZLUHItVSZIkSVJzLFYlSZIkSc2xWJUkSZIkNcdiVZIkSZLUHItVSZIkSVJz/n9leVFSG4REcgAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 1152x360 with 4 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "import numpy as np\n",
    "import matplotlib\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "fig,axes = plt.subplots(1, 4, figsize=(16, 5))\n",
    "\n",
    "for i,n in enumerate([100, 1000, 10000, 100000]):\n",
    "    p = [1/21, 2/21, 3/21, 4/21, 5/21, 6/21]\n",
    "    trials = np.random.choice(range(1, 7), size=n, p=p)\n",
    "    axes[i].hist(trials, bins=6, alpha=.5, range=(1, 7), density=True, rwidth=0.8)\n",
    "    axes[i].hlines(p, np.arange(1, 7), np.arange(2, 8), colors='red')\n",
    "    axes[i].set_title(f\"{n} trials\")\n",
    "    "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
