{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Statistical Hypothesis Testing\n",
    "\n",
    "Hypothesis testing is the process of taking 2 hypotheses and deciding which one is correct based on a sample statistic."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Basics\n",
    "\n",
    "Hypothesis testing is done by specifying two hypotheses about a population parameter - the **null hypothesis** and the **alternative hypothesis**. The alternative hypothesis is about something being different, having a certain effect, etc., while the null hypothesis generally states that there is no difference. The null hypothesis is written as $H_0$ while the alternative is written as $H_1$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The result of testing the two hypothesis is either **rejecting the null hypothesis** or **accepting the null hypothesis**. Rejecting it means that the null hypothesis is incorrect, while accepting it means that *we cannot say that it is incorrect*, however we do not know for sure if it is."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We decide whether to accept or reject the null hypothesis based on whether the sample statistic we get is rare or not. If it is, then we say that it is **significant** and so we reject the hypothesis, while if it is not, we accept it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also need to decide on a **level of significance**, that the probability of the value of statistic needs to meet in order to reject the null hypothesis. The interval below that level is called the **rejection region**, while the interval above it is called the **acceptance region**. The actual value of the statistic at the level of significance is called a **critical value**, while the statistic itself is called a **test statistic**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The level of significance can be interpreted as the CDF of the statistic at the critical value, which is the area under the curve up to that value. In contrast to that, the are under the curve up to the actual value of our statistic is called the **p-value**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## One-tailed and Two-tailed tests\n",
    "\n",
    "The above explanation applies to when we are doing a one-tailed test. However, with a two-tailed test, the rejection region would be split into two - the upper and lower part - and will thus cover half the area.\n",
    "\n",
    "Suppose that we were testing if the mean of some normally distributed population is less than the given $\\mu$  (the null hypothesis would be that the mean is $\\mu$), and we calculated the sample mean as $\\overline{X} = \\mu_1$. Since we know that $E(\\overline{X}) = \\mu$, we can check how \"off\" we are. In this case, $F(\\mu_1)$ (the CDF at $\\mu_1$) would be our *p-value*. Let's say that we chose a significance level of $5\\%$. If $F(\\mu_1) = 3\\% \\leq 5\\%$, then we reject the null hypothesis.\n",
    "\n",
    "However, let's say that we wanted to test if the mean is $\\mu$ or not. In this case, it would be appropriate to use a two-tailed test. In a two-tailed test, we would take the $2.5\\%$ and $97.5\\%$ points of the normal distribution as our critical values. Note that the significance level stays the same -  $2\\times2.5\\% = 5\\%$. In this case however, we would fail to reject the null hypothesis, since $2.5\\% \\leq 3\\% \\leq 97.5\\%$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "We can see that taking a different approach can change our conclusion entirely. It is important to be wise and not misuse these two approaches, especially doing a one-tailed test after a failed two-tailed test, since that would lead to wrong results with possible consequences."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Errors\n",
    "\n",
    "There are two types of errors in hypothesis testing:\n",
    "\n",
    "- Type I Error - rejecting the null hypothesis when it is true.\n",
    "- Type II Error - accepting the null hypothesis when the alternative hypothesis is true"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Type I Errors\n",
    "\n",
    "These are also known as **false positives**, since we are detecting something which is not there (recall that the alternative hypothesis suggest a *change*). The probability of a false positive is equal to the significance level and so the analyst is in control of this. If you want to reduce the risk of a false positive, you lower the p-value accordingly."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Type II Errors\n",
    "\n",
    "These are also known as **false negatives**. The probability of a false negative is written as $\\beta$, and the value $1 - \\beta$ is called the **power** of a test. Since $\\beta$ is dependent on the population, it is a probability that the analyst cannot control and thus has to estimate."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic Testing Examples\n",
    "\n",
    "Recall the confidence interval estimations from chapter 6. We derived a probability of the form:\n",
    "\n",
    "$$\n",
    "P(A_0 \\leq f(X) \\leq A_1)\n",
    "$$\n",
    "\n",
    "and then transformed it so that we get the estimate in the unknown parameter in the middle and the known values surrounding it.\n",
    "\n",
    "When testing, we plug the tested parameter in $f(X)$ and check if the probability of it is rare enough."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "Let us do a simple example. Say we have a class of 100 students, where each one took an exam and their scores \n",
    "turned out to obey a $N(50, 100)$ distribution.\n",
    "\n",
    "We now pretend not to know the mean and take a sample of 10 students, and compute a confidence interval on their scores:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[44.65358565 57.0494863 ]\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "import scipy.stats as sps\n",
    "\n",
    "N = 100\n",
    "SAMPLE_SIZE = 10\n",
    "rv = sps.norm(50, 10)\n",
    "population = rv.rvs(N)\n",
    "sample = np.random.choice(population, SAMPLE_SIZE)\n",
    "\n",
    "interval = np.mean(sample) - np.flip(sps.norm.interval(.95)) * np.sqrt(100 / 10)\n",
    "print(interval)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are $95\\%$ confident that the $44.65 \\leq \\mu \\leq 57.05$\n",
    "\n",
    "Now let us define our hypotheses:\n",
    "\n",
    "- $h_0: \\mu = 50$\n",
    "- $h_1: \\mu \\neq 50$\n",
    "\n",
    "Our test statistic is: $Z = (\\overline{X} - 50) / \\sqrt{\\frac{\\sigma^2}{n}}$\n",
    "\n",
    "We will *reject* the null hypothesis if $Z \\not\\in [z_{0.975}, z_{0.025}]$ and *accept* it otherwise."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Z = 0.2692793193157333\n",
      "z0.975 = -1.959963984540054\n",
      "z0.025 = 1.959963984540054\n"
     ]
    }
   ],
   "source": [
    "Z = (np.mean(sample) - 50) / np.sqrt(100 / SAMPLE_SIZE)\n",
    "z = sps.norm.interval(0.95)\n",
    "print(f\"Z = {Z}\")\n",
    "print(f\"z0.975 = {z[0]}\")\n",
    "print(f\"z0.025 = {z[1]}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since $Z \\in [z_{0.975}, z_{0.025}]$, we *accept* the null hypothesis - that is, we cannot say that it is untrue (and in fact it is true)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us do this for a range of values for $\\mu$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "95% Confidence interval is 44.65358565277187 - 57.04948629886311\n",
      "mu = 39.65358565277187 is rejected\n",
      "mu = 42.14201905789312 is rejected\n",
      "mu = 44.63045246301437 is rejected\n",
      "mu = 47.11888586813562 is accepted\n",
      "mu = 49.60731927325686 is accepted\n",
      "mu = 52.09575267837812 is accepted\n",
      "mu = 54.58418608349936 is accepted\n",
      "mu = 57.07261948862061 is rejected\n",
      "mu = 59.56105289374186 is rejected\n",
      "mu = 62.04948629886311 is rejected\n"
     ]
    }
   ],
   "source": [
    "print(f\"95% Confidence interval is {interval[0]} - {interval[1]}\")\n",
    "for mu in np.linspace(interval[0] - 5, interval[1] + 5, 10):\n",
    "    Z = (np.mean(sample) - mu) / np.sqrt(100 / SAMPLE_SIZE)\n",
    "    result = Z < z[0] or Z > z[1]\n",
    "    print(f\"mu = {mu} is {'rejected' if result else 'accepted'}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that the test obeys the confidence interval - that is, for values inside the confidence interval the test passes, and for values outside it fails."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "Assuming the level of significance is equal to one minus the confidence level of our estimation, hypothesis testing will always succeed for values inside the confidence interval."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Two-Sample Problem\n",
    "\n",
    "Now we are going to perform some tests on two populations. There are two ways of categorizing populations in this case - whether a normal distribution can be assumed (or not) and whether the data correspond to each other. In each of these cases we use a different test:\n",
    "\n",
    "\n",
    "| &nbsp;            | Normally Distributed | Non-Normally Distributed  |\n",
    "|-------------------|----------------------|---------------------------|\n",
    "|Correspondening    |Paired t-test       |Wilcoxon Signed-Rank test  |\n",
    "|Non-Corresponding  |Independent t-test  |Mann-Whitney U-test        |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Correspondence** means that the two data sets represent the same sample viewed from a different perspective (e.g. students' Math and English scores - each Math score has a corresponding English score). **Non-correspondence** means the inverse of the above (e.g. class A's Math scores and class B's Math scores - each student is different)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "### Paired t-test\n",
    "\n",
    "The paired t-test is used when the difference in values is normally distributed and we have a correspondence between data. \n",
    "\n",
    "Suppose we wanted to check if weight training improves concentration in a school's students. We take 20 random people and check their concentration level, make them train for 2 weeks and then check them again. How do we go about testing whether training has an effect or not?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Taking the mean before training as $\\mu_0$ and the mean after training as $\\mu_1$, we can formulated the following hypotheses:\n",
    "\n",
    "- $h_0: \\mu_1 - \\mu_0 = 0$\n",
    "- $h_1: \\mu_1 - \\mu_0 \\neq 0$\n",
    "\n",
    "If training has no effect on concentration, then the two distributions would be equal and thus $\\mu_1 = \\mu_0$. Therefore, we can take $\\mu_{diff} = \\mu_1 - \\mu_0$ and rewrite the hypotheses as:\n",
    "\n",
    "- $h_0: \\mu_{diff} = 0$\n",
    "- $h_1 : \\mu_{diff} \\neq 0$\n",
    "\n",
    "Now, if we assume that the difference is i.i.d. as $N(\\mu, \\sigma^2)$, this test turns into the **1-sample t-test** - that is, the test we use to check the mean of a normal distribution without knowing the variance, which uses the formula we derived for constructing a confidence interval."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "### Independent t-test\n",
    "\n",
    "The independent t-test is used to evaluate the difference in means when we do not have correspondence in the data.\n",
    "\n",
    "Suppose that the school we considered in the above example focuses mostly on art. Now let us consider another school, which focuses on sports. If weight training really did improve concentration, there should be a difference in the means of these two schools."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, we take the mean of the arts school as $\\mu_0$ and the mean of the sports school as $\\mu_1$. Our hypotheses are identical as well:\n",
    "\n",
    "- $h_0: \\mu_1 - \\mu_0 = 0$\n",
    "- $h_1: \\mu_1 - \\mu_0 \\neq 0$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we set the arts school's samples as $X_1,...X_n \\stackrel{i.i.d.}\\sim N(\\mu_0, \\sigma_0^2)$ and the sports school's samples as $Y_1,...Y_n \\stackrel{i.i.d.}\\sim N(\\mu_1, \\sigma_1^2)$, we can calculate the following $t$ statistic:\n",
    "\n",
    "$$\n",
    "t = \\frac{(\\overline{X} - \\overline{Y}) - (\\mu_0 - \\mu_2)}{\\sqrt{\\frac{s_0^2}{n_0} + \\frac{s_1^2}{n_1}}}\n",
    "$$\n",
    "\n",
    "Then we have the degrees of freedom as:\n",
    "\n",
    "$$\n",
    "\\nu = \\frac{\\Big(\\frac{s_0^2}{n_0} + \\frac{s_1^2}{n_1}\\Big)^2}{\\frac{s_0^4}{n_0^2(n_0 - 1)} + \\frac{s_1^4}{n_1^2(n_1 - 1)}}\n",
    "$$\n",
    "\n",
    "**NEEDS PROOF**\n",
    "\n",
    "We call this **Welch's method** or **Welch's t-test**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "### Wilcoxon Signed-Rank Test\n",
    "\n",
    "The Wilcoxon signed-rank test is used when we cannot assume a normal distribution in the *differences* in corresponding data. Given the hypotheses: \n",
    "\n",
    "- $h_0: \\mu_0 - \\mu_1 = 0$\n",
    "- $h_1: \\mu_0 - \\mu_1 \\neq 0$\n",
    "\n",
    "It works in the following way:\n",
    "\n",
    "1. Calculate the differences between corresponding data points - $D$\n",
    "2. Discard any $D_i = 0$\n",
    "3. Order all the differences by absolute value. Let $R_i$ be the rank of the $i$-th difference.\n",
    "4. Compute $T = \\sum_i{sgn(D_i)\\cdot R_i}$ for positive and negative ranks accordingly.\n",
    "5. Take $T$ as the smaller of $T^+$ and $T^-$\n",
    "\n",
    "Finally, in order to assess the significance of $T$, we need to consult a reference table, or approximate using a normal distribution, because $T^+ + T^-$ converges to a normal distribution when $n$ goes to $\\infty$.<sup>**NEEDS PROOF**</sup>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "### Mann-Whitney rank test\n",
    "\n",
    "The Mann-Whitney rank test, a.k.a. **Mann-Whitney U-test** or **Wilcoxon Rank-Sum test**, is used when we cannot assume a normally distributed poulations and there is no correspondence in data.\n",
    "\n",
    "Again, it is used to test the same hypotheses as the above, however the process is quite different:\n",
    "\n",
    "1. Assign a rank to all samples from both groups, beginning with 1 from the smallest value, where overlapping values are ranked as the midpoint of the rank range (e.g. if [3, 3, 3] were to be ranked as [4, 5, 6], we would assign 5 to all of them)\n",
    "2. Add up the ranks of data points in sample 1.\n",
    "3. Calculate $U_1 = R_1 - \\frac{n1(n1 + 1)}{2}$, where $n_1$ is the sample size of sample 1 and $R_1$ is the sum of the ranks in sample 1.\n",
    "4. Do this for the other sample and take the smaller $U$.\n",
    "\n",
    "After finding $U$, we need to consult a reference table for the significance."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "Note that we can simplify the above algorithm. We know that $R_1 + R_2 = \\frac{N(N+1)}{2}$ and $N = n_1 + n_2$. Let us calculate $U_1 + U_2$:\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "U_1 + U_2 &= R_1 + R_2 - \\bigg(\\frac{n_1(n_1 + 1)}{2} + \\frac{n_2(n_2 + 1)}{2}\\bigg) \\\\\n",
    "&= \\frac{N(N+1) - n_1(n_1 + 1) - n_2(n_2 + 1)}{2} \\\\\n",
    "&= \\frac{(n_1 + n_2)^2 + (n_1 + n_2) - n_1^2 - n_1 - n_2^2 - n_2}{2} \\\\\n",
    "&= \\frac{n_1^2 + 2n_1n_2 + n_2^2 - n_1^2 - n_2^2 }{2} \\\\\n",
    "&= n_1n_2\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "Therefore, if we know $U_1$, we can get $U_2$ as:\n",
    "\n",
    "$$\n",
    "U_2 = n_1n_2 - U_1\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Testing for Independence\n",
    "\n",
    "Consider a company that has made two versions of an ad - A and B - and wants to check if one of them makes people buy more of their products.\n",
    "\n",
    "In this case we use a **chi-squared test**. First, we define the hypotheses:\n",
    "\n",
    "- $h_0: $ $X$ (the ad a person saw) and $Y$ (whether they made a purchase) are independent.\n",
    "- $h_1: $ $X$ and $Y$ are not independent"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What we first need to do is calculate a **contingency table** or **cross tab**. For our example, a cross tab would look like this:\n",
    "\n",
    "| Ad seen | Purchased   | Did not purchase   |\n",
    "|---------|-------------|--------------------|\n",
    "| A       | $x$         | $y$                |\n",
    "| B       | $z$         | $w$                |\n",
    "\n",
    "where $x$, $y$, $z$ and $w$ are the **observed frequencies**. On the other hand, if we take $n_A$ and $n_B$ as the total people who saw the respective ads, and $m_1$ and $m_0$ as the total people who made or did not make a purchase, under the null hypothesis (independence) we would have the **expected frequencies** as:\n",
    "\n",
    "\n",
    "| Ad seen | Purchased           | Did not purchase   |\n",
    "|---------|---------------------|--------------------|\n",
    "| A       | $\\frac{1}{n}n_Am_1$ | $\\frac{1}{n}n_Am_0$ |\n",
    "| B       | $\\frac{1}{n}n_Bm_1$ | $\\frac{1}{n}n_Bm_0$ |\n",
    "\n",
    "The chi-squared test calculates the $Y$ statistic as:\n",
    "\n",
    "$$\n",
    "Y = \\sum_{i, j}\\frac{(O_{ij} - E_{ij})^2}{E_{ij}}\n",
    "$$\n",
    "\n",
    "$Y$ is known to converge to a $\\chi^2((r-1)(c-1))$ distribution, where $r$ and $c$ are the number of rows and columns respectively. \n",
    "\n",
    "After calculating $Y$, it is trivial to compute the $p$-value."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
