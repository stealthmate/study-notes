import matplotlib.colors as mplColor
from .GraphAnimation import GraphAnimation

STEPS = {
    'INIT': 'INIT',
    'INIT_SOURCE': 'INIT_SOURCE',
    'POP_VERTEX': 'POP_VERTEX',
    'DISCARD_EDGE': 'DISCARED_EDGE',
    'TRAVERSE_NEIGHBOUR': 'TRAVERSE_NEIGHBOUR',
    'ACCEPT_EDGE': 'ACCEPT_EDGE',
    'DROP_EDGE': 'DROP_EDGE',
    'VERTEX_FINISHED': 'VERTEX_FINISHED',
    'DONE': 'DONE'
}

class DijkstraAnimation(GraphAnimation):
    def __init__(self, V, adj, ax):
        E = set()
        for u, ns in adj.items():
            for v, w in ns:
                E.add((u, v, w))
        super(DijkstraAnimation, self).__init__(V, E, ax, {
            STEPS['INIT']: self.draw_init,
            STEPS['INIT_SOURCE']: self.draw_init_source,
            STEPS['POP_VERTEX']: self.draw_pop_vertex,
            STEPS['DISCARD_EDGE']: self.draw_discard_edge,
            STEPS['TRAVERSE_NEIGHBOUR']: self.draw_traverse_neighbour,
            STEPS['ACCEPT_EDGE']: self.draw_accept_edge,
            STEPS['DROP_EDGE']: self.draw_drop_edge,
            STEPS['VERTEX_FINISHED']: self.draw_vertex_finished,
            STEPS['DONE']: lambda x: ()
        })

        self.colors = {
            'NOT_SEEN': mplColor.to_rgba('lightgray'),
            'SEEING': mplColor.to_rgba('lightgreen'),
            'TRAVERSING': mplColor.to_rgba('salmon'),
            'SEEN': mplColor.to_rgba('lightblue'),
            'EDGE_DROPPED': mplColor.to_rgba('lightgray'),
            'EDGE_ACCEPTED': mplColor.to_rgba('black'),
            'EDGE_UNSEEN': mplColor.to_rgba('red')
        }

    def init(self):
        self.node_colors = [self.colors['NOT_SEEN'] for v in self.V]
        self.distance_labels = {}
        for n, (x, y) in self.pos.items():
            self.distance_labels[n] = t

    def draw_init(self, frame):
        self.node_colors = [self.colors['NOT_SEEN'] for v in self.V]
        self.edge_colors = [self.colors['EDGE_UNSEEN'] for e in self.E]
        self.setup_draw()

    def draw_init_source(self, frame):
        self.setNodeDistance(frame[1], 0)
    def draw_pop_vertex(self, frame):
        self.setNodeColor(frame[1], self.colors['SEEING'])
    def draw_traverse_neighbour(self, frame):
        _, n, nei = frame
        self.setNodeColor(nei, self.colors['TRAVERSING'])
    def draw_accept_edge(self, frame):
        _, n, nei, d, p = frame
        self.setNodeDistance(nei, str(d))
        self.setNodeColor(nei, self.colors['NOT_SEEN'])
        self.setEdgeColor(n, nei, self.colors['EDGE_ACCEPTED'])
        if p is not None:
            self.setEdgeColor(p, nei, self.colors['EDGE_DROPPED'])

    def draw_drop_edge(self, frame):
        _, n, nei = frame
        self.setNodeColor(nei, self.colors['NOT_SEEN'])
        self.setEdgeColor(n, nei, self.colors['EDGE_DROPPED'])

    def draw_discard_edge(self, frame):
        _, n, nei = frame
        self.setEdgeColor(n, nei, self.colors['EDGE_DROPPED'])

    def draw_vertex_finished(self, frame):
        _, n = frame
        self.setNodeColor(n, self.colors['SEEN'])
