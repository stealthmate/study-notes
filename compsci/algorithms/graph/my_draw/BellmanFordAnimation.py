import matplotlib.colors as mplColor
from .GraphAnimation import GraphAnimation

STEPS = {
    'INIT': 'INIT',
    'INIT_SOURCE': 'INIT_SOURCE',
    'BEGIN_ITERATION': 'BEGIN_ITERATION',
    'CHECK_EDGE': 'CHECK_EDGE',
    'ACCEPT_EDGE': 'ACCEPT_EDGE',
    'IGNORE_EDGE': 'IGNORE_EDGE',
    'END_RELAX': 'END_RELAX',
    'CHECK_EDGE_NEGATIVE': 'CHECK_EDGE_NEGATIVE',
    'THROW_NEGATIVE_CYCLE': 'THROW_NEGATIVE_CYCLE',
    'COMPLETE': 'COMPLETE'
}

class BellmanFordAnimation(GraphAnimation):
    def __init__(self, V, E, ax):
        super(BellmanFordAnimation, self).__init__(V, E, ax, {
            STEPS['INIT']: self.draw_init,
            STEPS['INIT_SOURCE']: self.draw_init_source,
            STEPS['BEGIN_ITERATION']: self.draw_begin_iteration,
            STEPS['CHECK_EDGE']: self.draw_check_edge,
            STEPS['ACCEPT_EDGE']: self.draw_accept_edge,
            STEPS['IGNORE_EDGE']: self.draw_ignore_edge,
            STEPS['END_RELAX']: self.draw_end_relax,
            STEPS['CHECK_EDGE_NEGATIVE']: self.draw_check_edge_negative,
            STEPS['THROW_NEGATIVE_CYCLE']: self.draw_throw_negative_cycle,
            STEPS['COMPLETE']: lambda x: ()
        })

        self.colors = {
            'EDGE_DROPPED': mplColor.to_rgba('lightgray'),
            'EDGE_ACCEPTED': mplColor.to_rgba('black'),
            'EDGE_UNSEEN': mplColor.to_rgba('blue'),
            'EDGE_CHECKING': mplColor.to_rgba('red')
        }

    def init(self):
        self.distance_labels = {}
        for n, (x, y) in self.pos.items():
            self.distance_labels[n] = t

    def draw_init(self, frame):
        self.node_colors = [mplColor.to_rgba('lightgray') for v in self.V]
        self.edge_colors = [self.colors['EDGE_DROPPED'] for e in self.E]
        artists = self.setup_draw()
        xlim = self.ax.get_xlim()
        ylim = self.ax.get_ylim()
        w = xlim[1] - xlim[0]
        h = ylim[1] - ylim[0]
        self.subtitle = self.ax.text(xlim[0] + w * 0.1, ylim[0] + h * 0.9, "", fontsize=15)
        return [*artists, self.subtitle]

    def draw_init_source(self, frame):
        self.setNodeDistance(frame[1], 0)
        return list(self.distance_labels.values())

    def draw_begin_iteration(self, frame):
        self.subtitle.set_text(f"Iteration {frame[1]}")
        return [self.subtitle]

    def draw_check_edge(self, frame):
        self.currentEdgeColor = self.getEdgeColor(frame[1], frame[2])
        self.setEdgeColor(frame[1], frame[2], self.colors['EDGE_CHECKING'])
        return [self.edges]

    def draw_accept_edge(self, frame):
        _, u, v, d, p = frame
        self.setNodeDistance(frame[2], d)
        self.setEdgeColor(frame[1], frame[2], self.colors['EDGE_ACCEPTED'])
        if p is not None:
            self.setEdgeColor(p, frame[2], self.colors['EDGE_DROPPED'])
        return [self.edges]

    def draw_ignore_edge(self, frame):
        self.setEdgeColor(frame[1], frame[2], self.currentEdgeColor)
        self.currentEdgeColor = None
        return [self.edges]

    def draw_end_relax(self, frame):
        self.subtitle.set_text("Relaxing done.")
        return [self.subtitle]

    def draw_check_edge_negative(self, frame):
        self.currentEdgeColor = self.getEdgeColor(frame[1], frame[2])
        self.setEdgeColor(frame[1], frame[2], self.colors['EDGE_CHECKING'])
        return [self.edges]

    def draw_throw_negative_cycle(self, frame):
        for u, v, w in self.E:
            self.setEdgeColor(u, v, self.colors['EDGE_CHECKING'])
        self.subtitle.set_text("Negative cycle found.")
        return [self.edges, self.subtitle]
