import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout

class GraphAnimation:
    def __init__(self, V, E, ax, drawMap):
        self.V = V
        self.E = set()
        for u, v, w in E:
            if (v, u, w) not in self.E:
                self.E.add((u, v, w))

        self.edgeIndex = { (u, v): i for i, (u, v, w) in enumerate(self.E) }

        self.ax = ax
        self.drawMap = drawMap

        self.G = nx.Graph()
        for u, v, w in self.E:
            self.G.add_edge(u, v, weight=w)

        self.pos = graphviz_layout(self.G, prog='sfdp')

    def findEdge(self, u, v):
        return self.edgeIndex.get((u, v), self.edgeIndex.get((v, u)))

    def setNodeColor(self, node, c):
        self.node_colors[node] = c
        self.nodes.set_facecolors(self.node_colors)

    def setNodeDistance(self, node, d):
        self.distance_labels[node].set_text(str(d))

    def getEdgeColor(self, u, v):
        return self.edge_colors[self.findEdge(u, v)]

    def setEdgeColor(self, u, v, c):
        e = self.findEdge(u, v)
        self.edge_colors[e] = c
        self.edges.set_color(self.edge_colors)

    def setup_draw(self):
        self.ax.clear()

        self.nodes = nx.draw_networkx_nodes(
            self.G,
            self.pos,
            nodelist=self.V,
            node_color=self.node_colors,
            node_size = 500,
            ax=self.ax)
        node_labels = nx.draw_networkx_labels(self.G, self.pos, ax=self.ax)

        xlim = self.ax.get_xlim()
        w = xlim[1] - xlim[0]
        ylim = self.ax.get_ylim()
        h = ylim[1] - ylim[0]

        self.distance_labels = {
            n: self.ax.text(x + 0.01*w, y + 0.03*h, "$\infty$", fontsize=15)
            for n, (x, y) in self.pos.items()
        }

        self.edges = nx.draw_networkx_edges(
            self.G,
            self.pos,
            edgelist=[(u, v) for u, v, w in self.E],
            edge_color=self.edge_colors,
            arrows=True,
            ax=self.ax)
        self.edges.set_linewidth(2)
        edge_labels = nx.draw_networkx_edge_labels(
            self.G,
            self.pos,
            edge_labels={ (u, v): w for u, v, w in self.E },
            font_size=15,
            ax=self.ax)

        def rescale(l, f):
            ofs = (l[1] - l[0]) * (f - 1)
            return (l[0] - ofs, l[1] + ofs)
        self.ax.set_xlim(rescale(self.ax.get_xlim(), 1.1))
        self.ax.set_ylim(rescale(self.ax.get_ylim(), 1.1))

        return [
            self.nodes,
            self.edges,
            *self.distance_labels.values(),
            *edge_labels.values(),
            *node_labels.values()
        ]

    def animate(self, frame):
        return self.drawMap[frame[0]](frame)
