{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction\n",
    "\n",
    "An algorithm is something which takes an **input** and produces an **output**.\n",
    "\n",
    "For example, the *Sorting problem* can be stated as follows:\n",
    "\n",
    "---\n",
    "\n",
    "**Input**: A sequence of numbers $\\langle a_1, a_2...a_n\\rangle$\n",
    "\n",
    "**Output**: A permutation of the sequence, such that $a_1\\prime \\leq a_2\\prime \\leq ... \\leq a_n\\prime$.\n",
    "\n",
    "---\n",
    "\n",
    "## Loop Invariance\n",
    "\n",
    "A loop invariant can be proven similarly to mathematical induction. We need to show the following:\n",
    "\n",
    "1. It is true prior to the first iteration.\n",
    "2. If it is true before an interation, then it is true before the next iteration\n",
    "3. When it terminates, it gives a property that can show the correct result\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "## Exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.1-3 Linear Search\n",
    "\n",
    "**Input**: A sequence of $n$ numbers $A = \\langle a_1,a_2,...,a_n\\rangle$ and a value $v$.\n",
    "\n",
    "**Output**: An index $i$ such that $v = A[i]$ or *NIL* if $v$ is not in $A$.\n",
    "\n",
    "Implementation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "def linearSearch(A, v):\n",
    "    result = None\n",
    "    for i in range(len(A)):\n",
    "        if v == A[i]:\n",
    "            result = i\n",
    "    return result"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can state the above loop as an invariant:\n",
    "> At the start of each iteration, `result` is either $k$ or `None` if $\\forall k < i, v \\neq A[k]$.\n",
    "\n",
    "And now let us prove that it is an invariant:\n",
    "\n",
    "1. Just before the first iteration, $i$ is $0$, `result` is `None` and since $A[0...i-1]$ is the empty set, the invariant is true.\n",
    "2. If $v \\not\\in A[0...i-1]$, then just before the $i+1$-th iteration, `result` would either be $i$ if $v = A[i]$ or `None` otherwise.\n",
    "3. After $n$ iterations, `result` contains either $k$ or `None`, which is the desired result."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "linearSearch(range(1, 10), 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "### 2.3-5 Binary Search\n",
    "\n",
    "**Input**: A sorted sequence of numbers $A$ of length $n$ and a key $v$.\n",
    "\n",
    "**Output**: An index $i$ of an element in $A$ if $v == A[i]$ or `None` if $v$ is not in $A$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 52,
   "metadata": {},
   "outputs": [],
   "source": [
    "def binarySearchRecursive(A, i, j, v):\n",
    "    if j - i == 0:\n",
    "        if v == A[i]:\n",
    "            return i\n",
    "        else:\n",
    "            return None\n",
    "    \n",
    "    midpoint_idx = (i + j) // 2\n",
    "    midpoint = (A[midpoint_idx] + A[midpoint_idx + 1]) / 2\n",
    "    if v <= A[midpoint_idx]:\n",
    "        return binarySearch(A, i, midpoint_idx, v)\n",
    "    else:\n",
    "        return binarySearch(A, midpoint_idx + 1, j, v)\n",
    "    \n",
    "def binarySearchIterative(A, v):\n",
    "    i = 0\n",
    "    j = len(A)\n",
    "    while i != j:\n",
    "        midpoint_idx = (i + j) // 2\n",
    "        if v <= A[midpoint_idx]:\n",
    "            j = midpoint_idx\n",
    "        else:\n",
    "            i = midpoint_idx + 1\n",
    "    if A[i] == v:\n",
    "        return i\n",
    "    else:\n",
    "        return None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to assess the worst-case performance, we will assume that $n = 2^k$. The worst-case is defined by the maximum number of splittings, which would occur when $v$ is either the first or last element in $A$. In that case, we need to split $A$ $k$ times (equivalent to dividing by $2^k$ in order to get a length of $1$), therefore the complexity is given by $\\Theta(k = \\lg n)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 53,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Recursive 5\n",
      "Iterative 5\n"
     ]
    }
   ],
   "source": [
    "A = range(11)\n",
    "v = 5\n",
    "print(\"Recursive\", binarySearchRecursive(A, 0, len(A), v))\n",
    "print(\"Iterative\", binarySearchIterative(A, v))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "### 2.3-7 Find a two-element sum\n",
    "\n",
    "**Input**: A set $S$ of $n$ integers and a number $x$.\n",
    "\n",
    "**Output:** Two elements $s_1$ and $s_2$ of $S$ such that $s_1 + s_2 = x$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 56,
   "metadata": {},
   "outputs": [],
   "source": [
    "def twoElementSum(S, x):\n",
    "    S = sorted(S) #O(nlg n)\n",
    "    for i in range(len(S)): # O(n)\n",
    "        s1 = S[i]\n",
    "        s2 = binarySearchIterative(S[i:], x - s1) # O(lg n)\n",
    "        if s2 != None and S[s2] != s1:\n",
    "            return s1, S[s2]\n",
    "    return None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First we sort the set into ascending order, which takes $\\Theta(n\\lg n)$ time. Then, traversing the sorted sequence, we take the current element to be $s_1$ and perform a binary search on the following elements in order to find $s_2$ if it exists. The worst case is when both $s_1$ and $s_2$ are at the end of $S$, where we need to compute $\\sum_i^n{\\lg i}$ operations. For the upper and lower bounds we have:\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "\\text{Upper} &= \\Theta\\Bigg[\\sum_i^n{\\lg n}\\Bigg] = \\Theta(n\\lg n) \\\\\n",
    "\\text{Lower} &= \\Theta\\Bigg[\\sum_{i = \\frac{n}{2}}^n{\\lg \\frac{n}{2}}\\Bigg] = \\Theta(\\frac{n}{2}\\lg \\frac{n}{2}) = \\Theta(n \\lg n)\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "Therefore the algorithm's worst-case complexity is $\\Theta(n\\lg n)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 59,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(1, 8)\n",
      "None\n"
     ]
    }
   ],
   "source": [
    "print(twoElementSum([1, 3, 8, 5], 9))\n",
    "print(twoElementSum([1, 3, 8, 5], 7))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "### 2-4 Inversions\n",
    "\n",
    "**Input**: An array $A$ of $n$ distinct numbers.\n",
    "\n",
    "**Output**: The number of inversions in $A$. An inversion is a pair of elements such that $i < j$ and $A[i] > A[j]$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 76,
   "metadata": {},
   "outputs": [],
   "source": [
    "def nInversions(A):\n",
    "    if len(A) == 1:\n",
    "        return 0, A\n",
    "    \n",
    "    i1, arr1 = nInversions(A[:len(A) // 2])\n",
    "    i2, arr2 = nInversions(A[len(A) // 2:])\n",
    "    \n",
    "    it = i1 + i2\n",
    "    arr = []\n",
    "    i = 0\n",
    "    j = 0\n",
    "    while i < len(arr1) and j < len(arr2):\n",
    "        if arr1[i] < arr2[j]:\n",
    "            arr.append(arr1[i])\n",
    "            i += 1\n",
    "        else:\n",
    "            arr.append(arr2[j])\n",
    "            it += 1\n",
    "            j += 1\n",
    "    if i < len(arr1):\n",
    "        arr.extend(arr1[i:])\n",
    "    elif j < len(arr2):\n",
    "        arr.extend(arr2[j:])\n",
    "    \n",
    "    return it, arr"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This only adds a constant number of operations to each step of merge sort so its complexity is the same - $\\Theta(n\\lg n)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 77,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(3, [1, 3, 5, 7, 8])\n"
     ]
    }
   ],
   "source": [
    "print(nInversions([5, 1, 3, 8, 7]))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
